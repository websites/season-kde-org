<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add on cascade constrain
 */
final class Version20220418171927 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $comment = $schema->getTable('comment');
        foreach ($comment->getForeignKeys() as $foreignKey) {
            if ('project' === $foreignKey->getForeignTableName()) {
                $comment->removeForeignKey($foreignKey->getName());
            }
        }

        $this->addSql('ALTER TABLE `comment` ADD CONSTRAINT FK_9474526C166D1F9N FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE;');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
