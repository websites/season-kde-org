<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Providers;

use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Tool\BearerAuthorizationTrait;
use Psr\Http\Message\ResponseInterface;

/**
 * A OAuth2 provide for MyKDE
 *
 * @package App\Providers
 */
class MyKdeProvider extends AbstractProvider
{
    use BearerAuthorizationTrait;

    /**
     * MyKDE API endpoint to retrieve logged in user information.
     *
     * @var string
     */
    public const PATH_API_USER = '/api/me';

    /**
     * MyKDE OAuth server authorization endpoint.
     *
     * @var string
     */
    public const PATH_API_AUTHORIZE = '/oauth/authorize';

    /**
     * MyKDE OAuth server token request endpoint.
     *
     * @var string
     */
    public const PATH_API_TOKEN = '/oauth/token';

    /**
     * Domain
     *
     * @var string
     */
    protected $domain;

    /**
     * @param array $options
     * @param array $collaborators
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(array $options = [], array $collaborators = [])
    {
        parent::__construct($options, $collaborators);
        if (empty($options['domain'])) {
            $message = 'The "domain" option not set. Please set a domain.';
            throw new \InvalidArgumentException($message);
        }
        $this->domain = $options['domain'];
    }

    /**
     * {@inheritDoc}
     */
    public function getBaseAuthorizationUrl()
    {
        return $this->domain . self::PATH_API_AUTHORIZE;
    }

    /**
     * {@inheritDoc}
     */
    public function getBaseAccessTokenUrl(array $params)
    {
        return $this->domain . self::PATH_API_TOKEN;
    }

    /**
     * {@inheritDoc}
     */
    public function getResourceOwnerDetailsUrl(AccessToken $token)
    {
        return $this->domain . self::PATH_API_USER;
    }

    /**
     * {@inheritDoc}
     */
    protected function getDefaultScopes()
    {
        return ['email'];
    }

    /**
     * {@inheritDoc}
     */
    protected function checkResponse(ResponseInterface $response, $data)
    {
    }

    /**
     * {@inheritDoc}
     */
    protected function createResourceOwner(array $response, AccessToken $token): MyKdeUser
    {
        return new MyKdeUser($response);
    }
}
