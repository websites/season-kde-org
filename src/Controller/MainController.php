<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Controller;

use App\Entity\User;
use App\Form\ProfileType;
use Doctrine\ORM\EntityManagerInterface;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    #[IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    #[Route(path: '/profile', name: 'profile')]
    public function profile(EntityManagerInterface $em, Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $form = $this->createForm(ProfileType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($user);
            $em->flush();
            $this->addFlash('info', 'You edited your address.');
            return $this->render('profile.html.twig', [
                'form' => $form->createView(),
            ]);
        }
        return $this->render('profile.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/', name: 'homepage')]
    public function index(EntityManagerInterface $em): Response
    {
        return $this->render('homepage.html.twig');
    }

    #[Route(path: '/connect/mykde', name: 'connect_mykde')]
    public function connectMyKde(ClientRegistry $clientRegistry): Response
    {
        return $clientRegistry
            ->getClient('mykde') // key used in config/packages/knpu_oauth2_client.yaml
            ->redirect([
                'email',
            ], []);
    }

    #[Route(path: '/connect/mykde/check', name: 'connect_mykde_check')]
    public function connectCheckAction(Request $request, ClientRegistry $clientRegistry): Response
    {
    }

    #[Route(path: '/logout', name: 'app_logout', methods: ['GET'])]
    public function logout(): Response
    {
        throw new \Exception('Don\'t forget to activate logout in security.yaml');
    }
}
