<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Controller;

use App\Entity\MentorApplication;
use App\Entity\User;
use App\Form\MentorApplicationType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class MentorApplicationController extends AbstractController
{
    #[IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    #[Route(path: '/mentor/application/create', name: 'mentor_application_create')]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($user->getMentorApplication() !== null) {
            throw new HttpException("You already submitted a mentor application.");
        }
        $mentorApplication = new MentorApplication();
        $mentorApplication->setUser($user);
        $form = $this->createForm(MentorApplicationType::class, $mentorApplication);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($mentorApplication);
            $em->flush();
            $request->getSession()->getFlashBag()->add('info', 'You submitted your mentor application.');
            return $this->redirectToRoute('profile');
        }
        return $this->render('mentor_application/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
