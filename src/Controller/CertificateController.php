<?php

// SPDX-FileCopyrightText: 2022 Carl Schwan <carl@carlschwan.eu>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

namespace App\Controller;

use App\Entity\Project;
use App\Entity\Season;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class CertificateController extends AbstractController
{
    #[Entity('Project', expr: 'repository.find(projectId)')]
    #[Route(path: '/{id}/project/{projectId}/certificate', name: 'certificate')]
    public function index(Season $season, Project $project, Pdf $snappy): PdfResponse
    {
        if (new \DateTime() < $season->getCertificatesIssued()) {
            throw new HttpException(403, "Certificates have not been issued yet");
        }
        if ($project->getCompleted() != true) {
            throw new HttpException(403, "You have not completed your project");
        }
        $html = $this->renderView('certificate.html.twig', [
            'season' => $season,
            'project' => $project
        ]);
        return new PdfResponse(
            $snappy->getOutputFromHtml($html, [
                'orientation' => 'landscape',
                'no-background' => false,
                'lowquality' => false,
                'page-height' => 238.125,
                'page-width' => 158.75,
                'encoding' => 'utf-8',
                'images' => true,
                'dpi' => 300,
                'enable-external-links' => true,
                'title' => 'certificate.pdf'
            ]),
            'certficate.pdf'
        );
    }
}
