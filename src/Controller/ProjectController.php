<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Project;
use App\Entity\Season;
use App\Entity\User;
use App\Form\CommentMentorType;
use App\Form\CommentStudentType;
use App\Form\ProjectMentorType;
use App\Form\ProjectStudentType;
use App\Form\ReplyType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ProjectController extends AbstractController
{
    #[IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    #[Route(path: '/project/new', name: 'project_new')]
    public function new(EntityManagerInterface $em, Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($user->isMentor()) {
            $flashBag->add('error', 'Mentor can\'t create new project');
            return $this->redirectToRoute('homepage');
        }
        $project = new Project();
        $form = $this->createForm(ProjectStudentType::class, $project);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $project->setStudent($user);
            $project->setAccepted(false);
            $project->setCompleted(false);
            $em->persist($project);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'New project saved');

            return $this->redirectToRoute('profile');
        }
        return $this->render('project/create.html.twig', [
            'message' => 'Create a new project',
            'form' => $form->createView(),
        ]);
    }

    #[IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    #[Route(path: '/project/{id}/edit', name: 'project_edit')]
    public function edit(Project $project, EntityManagerInterface $em, Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($user->getId() !== $project->getStudent()->getId()) {
            throw new UnauthorizedHttpException("You can only modify your projects");
        }
        $form = $this->createForm(ProjectStudentType::class, $project);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $project->setStudent($user);
            $project->setAccepted(false);
            $project->setCompleted(false);
            $em->persist($project);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'Project edited');

            return $this->redirectToRoute('profile');
        }
        return $this->render('project/create.html.twig', [
            'message' => 'Edit a project',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @IsGranted("IS_AUTHENTICATED_REMEMBERED")
     */
    #[Route(path: '/project/{id}/remove', name: 'project_remove')]
    public function removeProject(Project $project, Request $request, EntityManagerInterface $em): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if (!$project->getSeason()->isActive()) {
            throw new UnauthorizedHttpException("You can't remove a past project.");
        }
        if ($project->getStudent()->getId() !== $user->getId()) {
            throw new UnauthorizedHttpException("You can't only remove your own project.");
        }
        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($project);
            $em->flush();
            $request->getSession()->getFlashBag()->add('info', 'You deleted one of your project.');
            return $this->redirectToRoute('profile');
        }
        return $this->render('project/mentor.html.twig', [
            'message' => "Do you really want to remove this project",
            'button' => "Remove",
            'state' => 'danger',
            'form' => $form->createView(),
        ]);
    }

    #[IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    #[Route(path: '/project/mentor/{id}', name: 'project_mentor')]
    public function addMentor(Project $project, Request $request, EntityManagerInterface $em): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if (!$user->isMentor()) {
            throw new UnauthorizedHttpException("Only mentor can mentor a project");
        }
        if (!$project->getSeason()->isActive()) {
            throw new UnauthorizedHttpException("You can't mentor a past project.");
        }
        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $project->addMentor($user);
            $em->persist($project);
            $em->flush();
            $request->getSession()->getFlashBag()->add('info', 'Now mentoring ' . $project->getTitle());
            return $this->redirectToRoute('profile');
        }
        return $this->render('project/mentor.html.twig', [
            'message' => "Do you really want to mentor this project",
            'button' => "Mentor",
            'state' => 'success',
            'form' => $form->createView(),
        ]);
    }

    #[IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    #[Route(path: '/project/stop-mentor/{id}', name: 'project_stop_mentor')]
    public function removeMentor(Project $project, Request $request, EntityManagerInterface $em): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if (!$user->isMentor()) {
            throw new UnauthorizedHttpException("Only mentor can mentor a project");
        }
        if (!$project->getSeason()->isActive()) {
            throw new UnauthorizedHttpException("You can't stop mentoring a past project.");
        }
        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $project->removeMentor($user);
            $em->persist($project);
            $em->flush();
            $request->getSession()->getFlashBag()->add('info', 'Now mentoring ' . $project->getTitle());
            return $this->redirectToRoute('profile');
        }
        return $this->render('project/mentor.html.twig', [
            'message' => "Do you want to stop mentoring this project",
            'button' => "Stop mentoring",
            'state' => 'warning',
            'form' => $form->createView(),
        ]);
    }

    #[IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    #[Route(path: '/projects', name: 'projects')]
    public function list(EntityManagerInterface $em): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if (!$user->isMentor()) {
            throw new UnauthorizedHttpException("Only mentor can mentor a project");
        }
        $activeSeason = $em->getRepository(Season::class)
            ->findOneBy(['active' => true]);
        if (!$activeSeason instanceof Season) {
            throw new HttpException("No active season, please contact admin, if you think this is an error.");
        }
        return $this->render('project/list.html.twig', [
            'projects' => $activeSeason->getProjects(),
            'season' => $activeSeason,
        ]);
    }

    #[IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    #[Route(path: '/project/{id}', name: 'project_view')]
    public function view(Project $project, EntityManagerInterface $em, Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $activeSeason = $em->getRepository(Season::class)
            ->findOneBy(['active' => true]);
        if (!$activeSeason instanceof Season) {
            throw new HttpException("No active season, please contact admin, if you think this is an error.");
        }
        $isAdminOrMentor = $user->isMentor() || in_array('ROLE_ADMIN', $user->getRoles());
        if (!$isAdminOrMentor && $user->getId() !== $project->getStudent()->getId()) {
            throw new UnauthorizedHttpException("You can only view your projects");
        }
        $canReply = (!$project->getAccepted() && $project->getSeason()->getDeadlineStudent() < new \DateTime('now')) || $isAdminOrMentor;
        $repo = $em->getRepository(Comment::class);
        $comments = $isAdminOrMentor ? $repo->findBy([
            'project' => $project,
            'level' => 0,
        ]) : $repo->findBy([
            'project' => $project,
            'level' => 0,
            'mentor' => 0
        ]);

        $replyForm = $this->createForm(ReplyType::class);
        $replyForm->handleRequest($request);
        if ($replyForm->isSubmitted() && $replyForm->isValid()) {
            if (!$canReply) {
                throw new UnauthorizedHttpException("You can't reply anymore");
            }
            $reply = new Comment();
            $reply->setProject($project);
            $reply->setAuthor($user);
            $reply->setMessage($replyForm->get('message')->getData());
            $parentId = $replyForm->get('parent')->getData();
            $parent = $repo->find($parentId);
            $repo->persistAsLastChildOf($reply, $parent);
            $em->flush();
            $request->getSession()->getFlashBag()->add('success', 'Successfully replied to comment');

            return $this->redirectToRoute('project_view', ['id' => $project->getId()]);
        }

        $commentForm = $this->createForm($isAdminOrMentor ? CommentMentorType::class : CommentStudentType::class);
        $commentForm->handleRequest($request);
        if ($commentForm->isSubmitted() && $commentForm->isValid()) {
            if (!$canReply) {
                throw new UnauthorizedHttpException("You can't comment anymore");
            }
            $comment = new Comment();
            $comment->setProject($project);
            $comment->setAuthor($user);
            $comment->setMessage($commentForm->get('message')->getData());
            if ($isAdminOrMentor) {
                $comment->setMentor(!($commentForm->get('mentor')->getData()));
            }
            $em->persist($comment);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', 'Comment posted successfully');

            return $this->redirectToRoute('project_view', ['id' => $project->getId()]);
        }

        $params = [
            'project' => $project,
            'replyForm' => $replyForm->createView(),
            'commentForm' => $commentForm->createView(),
            'comments' => $comments,
            'canReply' => $canReply,
        ];

        $workFinished = new \DateTime() > $project->getSeason()->getEndWork();
        if ($isAdminOrMentor && $workFinished) {
            $mentorFeedbackForm = $this->createForm(ProjectMentorType::class, $project);
            $mentorFeedbackForm->handleRequest($request);

            if ($mentorFeedbackForm->isSubmitted() && $mentorFeedbackForm->isValid()) {
                $em->persist($project);
                $em->flush();
                $request->getSession()->getFlashBag()->add('success', 'Feedback sent!');
                return $this->redirectToRoute('project_view', ['id' => $project->getId()]);
            }
            $params['mentorForm'] = $mentorFeedbackForm->createView();
        }

        return $this->render('project/view.html.twig', $params);
    }


    #[Route(path: '/{id}/projects/accepted', name: 'projects_accepted')]
    public function accepted(Season $season, EntityManagerInterface $em): Response
    {
        /** @var Project $acceptedProjects */
        $acceptedProjects = $em->getRepository(Project::class)->findBy([
            "season" => $season,
            "accepted" => true,
            "isVisible" => true,
        ]);
        return $this->render('project/accepted.html.twig', [
            'projects' => $acceptedProjects,
            'season' => $season,
        ]);
    }

    #[IsGranted('IS_AUTHENTICATED_REMEMBERED')]
    #[Route(path: '/project/hide/{id}', name: 'project_hide_accepted')]
    public function hideAccepted(Project $project, Request $request, EntityManagerInterface $em): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if ($user->isMentor()) {
            throw new UnauthorizedHttpException("Only student can hide or unhide project");
        }
        if ($user->getId() !== $project->getStudent()->getId()) {
            throw new UnauthorizedHttpException("You can only modify your projects");
        }
        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $isVisible = $project->getIsVisible();
            $project->setIsVisible(!$isVisible);
            $em->persist($project);
            $em->flush();
            $request->getSession()->getFlashBag()->add('info', $project->getTitle() . ' is now' . ($isVisible ? ' hidden from' : ' shown in') . ' the list of accepted projects');
            return $this->redirectToRoute('profile');
        }
        return $this->render('project/mentor.html.twig', [
            'message' => "Do you want to " . ($project->getIsVisible() ? "hide" : "show") . " this project from the list of accepted projects",
            'button' => ($project->getIsVisible() ? "Hide" : "Show"),
            'state' => 'warning',
            'form' => $form->createView(),
        ]);
    }
}
