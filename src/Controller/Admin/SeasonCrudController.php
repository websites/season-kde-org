<?php

// SPDX-FileCopyrightText: 2022 Carl Schwan <carl@carlschwan.eu>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

namespace App\Controller\Admin;

use App\Entity\Season;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class SeasonCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Season::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Season')
            ->setEntityLabelInPlural('Season')
            ->setSearchFields(['id', 'title', 'description']);
    }

    public function configureFields(string $pageName): iterable
    {
        $title = TextField::new('title');
        $description = TextareaField::new('description');
        $startTime = DateTimeField::new('startTime');
        $active = BooleanField::new('active');
        $deadlineStudent = DateTimeField::new('deadlineStudent');
        $deadlineMentor = DateTimeField::new('deadlineMentor');
        $projectsAnnouncement = DateTimeField::new('projectsAnnouncement');
        $startWork = DateTimeField::new('startWork');
        $endWork = DateTimeField::new('endWork');
        $resultAnnouncement = DateTimeField::new('resultAnnouncement');
        $certificatesIssued = DateTimeField::new('certificatesIssued');
        $projects = AssociationField::new('projects');
        $id = IntegerField::new('id', 'ID');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $title, $startTime, $active, $deadlineStudent, $deadlineMentor, $projectsAnnouncement];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $title, $description, $startTime, $active, $deadlineStudent, $deadlineMentor, $projectsAnnouncement, $startWork, $endWork, $resultAnnouncement, $certificatesIssued, $projects];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$title, $description, $startTime, $active, $deadlineStudent, $deadlineMentor, $projectsAnnouncement, $startWork, $endWork, $resultAnnouncement, $certificatesIssued, $projects];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$title, $description, $startTime, $active, $deadlineStudent, $deadlineMentor, $projectsAnnouncement, $startWork, $endWork, $resultAnnouncement, $certificatesIssued, $projects];
        }
    }
}
