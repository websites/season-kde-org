<?php

// SPDX-FileCopyrightText: 2022 Carl Schwan <carl@carlschwan.eu>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('User')
            ->setEntityLabelInPlural('Users')
            ->setSearchFields(['id', 'myKdeId', 'username', 'roles']);
    }

    public function configureFields(string $pageName): iterable
    {
        $myKdeId = TextField::new('myKdeId');
        $username = TextField::new('username');
        $mentor = BooleanField::new('mentor');
        $projects = AssociationField::new('projects');
        $projectsMentored = AssociationField::new('projectsMentored');
        $mentorApplication = AssociationField::new('mentorApplication');
        $id = IntegerField::new('id', 'ID');
        $roles = ArrayField::new('roles');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $myKdeId, $username, $mentor, $projects, $projectsMentored, $mentorApplication];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $myKdeId, $username, $roles, $mentor, $projects, $projectsMentored, $mentorApplication];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$myKdeId, $username, $mentor, $projects, $projectsMentored];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$myKdeId, $username, $mentor, $projects, $projectsMentored];
        }
    }
}
