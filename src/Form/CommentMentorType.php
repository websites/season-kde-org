<?php
/**
 * SPDX-FileCopyrightText: 2020 Anuj Bansal <bansalanuj9@gmail.com>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Form;

use App\Entity\Comment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentMentorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('message', TextareaType::class, ['label' => false, 'attr' => ['style' => 'min-height:100px']])
            ->add('mentor', CheckboxType::class, ['label' => 'Visible to Student', 'data' => true, 'required' => false])
            ->add('post', SubmitType::class, ['label' => 'Post Comment']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comment::class,
        ]);
    }
}
