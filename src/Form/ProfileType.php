<?php

// SPDX-FileCopyrightText: 2022 Carl Schwan <carl@carlschwan.eu>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('address', TextareaType::class, [
                'label' => 'Address for SWAG and certificate (only visible to SoK admins and optional)',
                'required' => false,
            ])
            ->add('tShirt', ChoiceType::class, [
                'label' => 'T-Shirt size for SWAG',
                'choices' => [
                    'S (fitter)' => 'S (fitter)',
                    'M (fitter)' => 'M (fitter)',
                    'L (fitter)' => 'L (fitter)',
                    'XL (fitter)' => 'XL (fitter)',
                    'XLL (fitter)' => 'XXL (fitter)',
                    'S' => 'S',
                    'M' => 'M',
                    'L' => 'L',
                    'XL' => 'XL',
                    'XL' => 'XXL',
                ],
                'required' => false,
            ])
            ->add('submit', SubmitType::class, ['label' => 'Save address']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
