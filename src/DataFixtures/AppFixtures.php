<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\DataFixtures;

use App\Entity\Season;
use Doctrine\Bundle\FixturesBundle\Fixture;

use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $season = new Season();
        $season->setTitle("Season of KDE 2025");
        $season->setDescription("A wonderful season for contributing to KDE");
        $season->setStartTime(new \DateTime());
        $season->setActive(true);
        $season->setDeadlineStudent(new \DateTime());
        $season->setDeadlineMentor(new \DateTime());
        $season->setProjectsAnnouncement(new \DateTime());
        $season->setStartWork(new \DateTime());
        $season->setEndWork(new \DateTime());
        $season->setResultAnnouncement(new \DateTime());
        $season->setCertificatesIssued(new \DateTime());


        $manager->persist($season);

        $manager->flush();
    }
}
