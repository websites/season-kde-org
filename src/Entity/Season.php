<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Entity;

use App\Repository\SeasonRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SeasonRepository::class)]
class Season implements \Stringable
{
    #[ORM\Id, ORM\GeneratedValue, ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::STRING, length: 255)]
    private string $title = '';

    #[ORM\Column(type: Types::TEXT)]
    private string $description = '';

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private \DateTimeInterface $startTime;

    #[ORM\Column]
    private bool $active = false;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private \DateTimeInterface $deadlineStudent;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private \DateTimeInterface $deadlineMentor;

    #[ORM\OneToMany(mappedBy: 'season', targetEntity: Project::class, orphanRemoval: true)]
    private Collection $projects;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private \DateTimeInterface $projectsAnnouncement;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private \DateTimeInterface $startWork;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private \DateTimeInterface $endWork;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private \DateTimeInterface $resultAnnouncement;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private \DateTimeInterface $certificatesIssued;

    public function __construct()
    {
        $this->projects = new ArrayCollection();
        $this->startTime = new DateTime();
        $this->startWork = new DateTime();
        $this->endWork = new DateTime();
        $this->resultAnnouncement = new DateTime();
        $this->certificatesIssued = new DateTime();
        $this->deadlineMentor = new DateTime();
        $this->deadlineStudent = new DateTime();
    }

    public function __toString(): string
    {
        return (string) $this->title;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStartTime(): ?\DateTimeInterface
    {
        return $this->startTime;
    }

    public function setStartTime(\DateTimeInterface $startTime): self
    {
        $this->startTime = $startTime;

        return $this;
    }


    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getDeadlineStudent(): ?\DateTimeInterface
    {
        return $this->deadlineStudent;
    }

    public function setDeadlineStudent(\DateTimeInterface $deadlineStudent): self
    {
        $this->deadlineStudent = $deadlineStudent;

        return $this;
    }

    public function getDeadlineMentor(): ?\DateTimeInterface
    {
        return $this->deadlineMentor;
    }

    public function setDeadlineMentor(\DateTimeInterface $deadlineMentor): self
    {
        $this->deadlineMentor = $deadlineMentor;

        return $this;
    }

    /**
     * @return Collection|Project[]
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
            $project->setSeason($this);
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->contains($project)) {
            $this->projects->removeElement($project);
            // set the owning side to null (unless already changed)
            if ($project->getSeason() === $this) {
                $project->setSeason(null);
            }
        }

        return $this;
    }

    public function getProjectsAnnouncement(): ?\DateTimeInterface
    {
        return $this->projectsAnnouncement;
    }

    public function setProjectsAnnouncement(\DateTimeInterface $projectsAnnouncement): self
    {
        $this->projectsAnnouncement = $projectsAnnouncement;

        return $this;
    }

    public function getStartWork(): ?\DateTimeInterface
    {
        return $this->startWork;
    }

    public function setStartWork(\DateTimeInterface $startWork): self
    {
        $this->startWork = $startWork;

        return $this;
    }

    public function getEndWork(): ?\DateTimeInterface
    {
        return $this->endWork;
    }

    public function setEndWork(\DateTimeInterface $endWork): self
    {
        $this->endWork = $endWork;

        return $this;
    }

    public function getResultAnnouncement(): ?\DateTimeInterface
    {
        return $this->resultAnnouncement;
    }

    public function setResultAnnouncement(\DateTimeInterface $resultAnnouncement): self
    {
        $this->resultAnnouncement = $resultAnnouncement;

        return $this;
    }

    public function getCertificatesIssued(): ?\DateTimeInterface
    {
        return $this->certificatesIssued;
    }

    public function setCertificatesIssued(\DateTimeInterface $certificatesIssued): self
    {
        $this->certificatesIssued = $certificatesIssued;

        return $this;
    }
}
