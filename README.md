# Season of KDE website

Allow mentor and mentee to register to the Season of KDE program.

## Deployment

1. Clone this reptository
2. Configure apache like show on https://symfony.com/doc/current/setup/web_server_configuration.html
3. Install composer, php7.2 (or later), and wkhtmltopdf
4. Create new mysql database
5. `composer dump-env prod` and edit `.env.local.php`
6. `php bin/console doctrine:migrations:migrate`
6. Make var folder writable by server

## Credits

This application was developed by Anuj Bansal and Carl Schwan.

## License

This application is licensed under AGPL-3.0-or-later. See LICENSES folder for more informations.
