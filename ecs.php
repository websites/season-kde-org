<?php
# SPDX-FileCopyrightText: Carl Schwan <carl@carlschwan.eu>
#
# SPDX-License-Identifier: CC0-1.0
use Symplify\EasyCodingStandard\Config\ECSConfig;
use Symplify\EasyCodingStandard\ValueObject\Set\SetList;
use PhpCsFixer\Fixer\Import\NoUnusedImportsFixer;
use PhpCsFixer\Fixer\ArrayNotation\ArraySyntaxFixer;

return static function (ECSConfig $ecsConfig): void {
    // A. full sets
    $ecsConfig->sets([SetList::PSR_12]);

    $ecsConfig->ruleWithConfiguration(ArraySyntaxFixer::class, [
        'syntax' => 'short',
    ]);
    $ecsConfig->rule(NoUnusedImportsFixer::class);
};
